## Université OpenClassrooms

**Formation :** Développeur d'application - PHP/Symfony.

**Cours :** Développez votre site web avec le framework Symfony.

**Dirigé par :** Professeur Alexandre Baco / Fabien Potencier.

**Exercice :** créez un système pour nettoyer vos entités.

**URL du cours :** https://openclassrooms.com/fr/courses/3619856-developpez-votre-site-web-avec-le-framework-symfony

**Travail réalisé par:** Luciano Goulart [lgoulart@lnx-it.inf.br].


## Installation

```
composer install;
```
