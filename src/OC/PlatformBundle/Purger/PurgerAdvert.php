<?php
namespace OC\PlatformBundle\Purger;

use Doctrine\ORM\EntityManagerInterface;

class PurgerAdvert
{
  private $em;

  public function __construct(EntityManagerInterface $em)
  {
    // Entity Manager
    $this->em = $em;
  }

  function purge($days)
  {
    // Get the repository
    $advertRepository = $this->em->getRepository('OCPlatformBundle:Advert');
    $advertSkillRepository = $this->em->getRepository('OCPlatformBundle:AdvertSkill');

    // Get the list of the publications to purge
    $listAdverts = $advertRepository->getAdvertsToPurge(new \Datetime('-'.$days.' day'));

    // Routine to delete the publications
    foreach ($listAdverts as $advert)
    {
      // Delete AdvertSkill
      $advertSkills = $advertSkillRepository->findByAdvert($advert);
      foreach ($advertSkills as $advertSkill)
      {
        $this->em->remove($advertSkill);
      }

      // Delete the publication
      $this->em->remove($advert);

    }
    // Flush
    $this->em->flush();
  }
}
